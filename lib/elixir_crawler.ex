defmodule ElixirCrawler do
  use HTTPoison.Base
  def crawl(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        block_job(body)
      {:ok, %HTTPoison.Response{status_code: 301}} ->
        IO.puts "Not found"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
  end

  # to do
  # Passar um block de vaga de cada vez
  defp block_job(body) do
    block = Floki.parse_document!(body)
    |> Floki.find(".jobsearch-SerpJobCard")

    for position <- 1..Enum.count(block) do
      current_block = Enum.at(position, block)
      IO.inspect(current_block)
      contact_job_string(position, block)
    end
  end

  defp contact_job_string(position, block) do
    job_text = find_title(position, block)
    job_company = find_company(block)
    job_city = find_city(block)
    job_salary = find_salary(block)
    job = [ ]
    vaga = [
      Enum.at(job_text, position),
      Enum.at(job_company, position),
      Enum.at(job_city, position),
      Enum.at(job_salary, position)
    ]
    job ++ vaga
  end

  defp find_title(position, block) do
    title = Floki.find(block, ".jobsearch-SerpJobCard h2.title a")
    |> Floki.attribute("title")
  end

  defp find_company(block) do
    Floki.find(block, ".jobsearch-SerpJobCard .sjcl div .company")
    |> Floki.text
    |> String.split("\n")
  end

  defp find_city(block) do
    Floki.find(block, ".jobsearch-SerpJobCard .sjcl div")
    |> Floki.attribute("data-rc-loc")
  end

  defp find_salary(block) do
    Floki.find(block, ".jobsearch-SerpJobCard .salarySnippet .salary .salaryText")
    |> Floki.text
    |> String.split("\n")
  end
end
